Feature: User attempts login

    Scenario: Login with invalid user
    GIVEN A user is on the login page
    WHEN This user logs in with invalid credentials
    THEN An error message should show