package stepDefinitions;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertEquals;


public class example_apiTestSteps_2 {
    String apiUrl = "https://jsonplaceholder.typicode.com/todos/";
    HttpResponse<JsonNode> firstTodoResponse;

    @Given("^A user is on the login page$")
    public void a_user_is_on_the_login_page() throws UnirestException {
        HttpResponse<JsonNode> todosResponse = Unirest.get(apiUrl).asJson();

        assertEquals(200, todosResponse.getStatus());
    }

    @When("^This user logs in with invalid credentials$")
    public void this_user_logs_in_with_invalid_credentials() throws  UnirestException {
        firstTodoResponse = Unirest.get(apiUrl + "1").asJson();

        assertEquals(200, firstTodoResponse.getStatus());
    }

    @Then("^An error message should show$")
    public void an_error_message_should_show() {
        String expectedResponse = "{\"id\":1,\"completed\":false,\"title\":\"delectus aut autem\",\"userId\":1}";
        String receivedResponse = firstTodoResponse.getBody().getArray().get(0).toString();

        assertEquals(expectedResponse,receivedResponse);

    }
}